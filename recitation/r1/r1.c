#include <stdio.h>
#include <stdlib.h>

void increaseA(int val)   {   val++;   }
void increaseB(int *pval) {   pval++;  }
void increaseC(int *pval) {  *pval++;  }
void increaseD(int *pval) { (*pval)++; }

int main(int argc, char const *argv[]) {

  int a,b,c,temp,sum,*pa,*pb,*pc, **ppa,**ppb, **ppc, *tp;

  //initialization
  a = 10;
  b = 20;
  c = 30;

  // single depth pointer initialization
  pa = &a;
  pb = &b;
  pc = &c;

  // double depth pointer initialization
  ppa = &pa;
  ppb = &pb;
  ppc = &pc;

  // print a, b, c, direct and indirect
  printf ("\nDirect print of a, b, c\t\t:\t%d\t%d\t%d\n",a,b,c);
  printf ("Indirect print of a, b, c\t:\t%d\t%d\t%d\n",*pa,*pb,*pc);
  printf ("Double Indirect print of a, b, c:\t%d\t%d\t%d\n",**ppa,**ppb,**ppc);

  sum = a + *pb + **ppc;

  printf("\nThe sum is  \t\t\t:\t%d\n",sum);

  *pa   = *pa ^ **ppb;
  **ppb = *pa ^ **ppb;
  *pa   = *pa ^ **ppb;

  printf("Swapped A and B are  \t\t:\t%d\t%d\n",a,b);

  tp = pa;
  pa = *ppb;
  *ppb = tp;

  printf("\n");

  printf("Swapped pa and pb values of a, b :\t%d\t%d\n",a,b);
  printf("Swapped printed indirect for a, b:\t%d\t%d\n",*pa,*pb );
  // we changed the addresses of the pointers, but never changed the values
  // the pointers pointed to
  // Thus, a will print 20, b 10, as they were changed in the previous Swapped
  // but when printed indirect, pa points to b, whereas pb points to a!

  printf("\n");

  // no change as passed by value, temp version in func dies in func
  increaseA(a);
  printf("Value of A after increaseA\t:\t%d\n",a);

  // increases the address, not the value, the address dies in the func
  increaseB(&a);
  printf("Value of A after increaseB\t:\t%d\n",a);

  // order of precidence.  Increases the address, then derefs.
  increaseC(&a);
  printf("Value of A after increaseC\t:\t%d\n",a);

  // with (), derefs, then increases value pointed at via the pointer
  // since it's modifying the original location, the value changes in main
  increaseD(&a);
  printf("Value of A after increaseD\t:\t%d\n",a);

  // exit
  printf("\n\n" );
  return 0;
}
