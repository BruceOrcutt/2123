typedef  int Element;

// NodeLL typedef
typedef struct NodeLL
{
    Element element;
    struct NodeLL *pNext;	// points to next node
} NodeLL;

// LinkedListImp typedef
typedef struct
{
    NodeLL *pHead;	// pointer to first node in linked list
} LinkedListImp;

typedef LinkedListImp *LinkedList;

LinkedList newLinkedList ()
{
    LinkedList list = (LinkedList) malloc(sizeof(LinkedListImp));
    // Mark the list as empty
    list->pHead = NULL;   // empty list
    return list;
} 

NodeLL *searchLL(LinkedList list, Key match, NodeLL **ppPrecedes)
{
    NodeLL *p;
    // used when the list is empty or we need to insert at the beginning
    *ppPrecedes = NULL;

    // Traverse through the list looking for where the key belongs or
    // the end of the list.
    for (p = list->pHead; p != NULL; p = p->pNext)
    {
        if (match == p->element.key)
            return p;
        if (match < p-> element.key)
            return NULL;
        *ppPrecedes = p;
    }

NodeLL *insertOrderedLL(LinkedList list, Element value)
{
    NodeLL *pNew, *pFind, *pPrecedes;
    // see if it already exists
    // Notice that we are passing the address of pPrecedes so that 
    // searchLL can change it.
    pFind = searchLL(list, value.key, &pPrecedes);
    if (pFind != NULL)
        return pFind;

    // doesn't already exist.  Allocate a node and insert.
    pNew = allocateNode(list, value);

    // Check for inserting at the beginning of the list
    // this will also handle when the list is empty
    if (pPrecedes == NULL)
    {
        pNew->pNext = list->pHead;
        list->pHead = pNew;
    }
    else
    {
        pNew->pNext = pPrecedes->pNext;
        pPrecedes->pNext = pNew;
    }
    return pNew;

NodeLL *allocateNode(LinkedList list, Element value)
{
    NodeLL *pNew;
    pNew = (NodeLL *)malloc(sizeof(NodeLL));
    if (pNew == NULL) {
        printf( "No available memory for linked list\n");
	exit -1;
	}
    pNew->element = value;
    pNew->pNext = NULL;
    return pNew;
}

