#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

#define ASIZE 50
#define BSIZE 70
#define CSIZE 120

void set_array_rand(int x[], int n) {
	int i;
	for(i=0;i<n;i++) {
		x[i]= rand_int(30,100);
	}
}

int rand_int(int a,int b) {

	return rand()%(b-a+1) * a;

}

void selection_sort(int x[], int n)
{
	int k,j,m,i;
	int temp = INT_MAX;
	
	for(j=0;j<n-1;j++)
		for(i=j;i<n;i++) {
			if (x[i] < x[j]) {
				// if lower, swap
				x[i] = x[i] ^ x[j];
				x[j] = x[i] ^ x[j];
				x[i] = x[i] ^ x[j];
			} 
		} 
		
	
}


void merge(int a[], int na, int b[],int nb, int c[],int nc) {
	int i = 0;
	int j = 0;
	int k = 0;

	// merge a and b into c.  
	// a and b are assumed to be sorted

	while ((i < na) && (j < nb)) {

		if(a[i]<b[j]) {
			c[k] = a[i];
			i++;
		} else {
			c[k] = b[j];
			j++;
		}

		k++;
	}
	if (i<na) {
		while(i < na) {
			c[k] = a[i];
			i++;
			k++;
		} 
	
	}	else {
		while(j < nb) {
			c[k] = b[j];
			j++;
			k++;
		}
		
	}	
}


void print_array(int x[],int n) {

	int i;

	for(i=0;i<n;i++) 
		printf("%d\n",x[i]);
}

int main() {
	int	a[ASIZE],
		b[BSIZE],
		c[CSIZE];

	set_array_rand(a,ASIZE);
	set_array_rand(b,BSIZE);
	selection_sort(a,ASIZE);	
	selection_sort(b,BSIZE);
	
	merge(a,ASIZE,b,BSIZE,c,CSIZE);
	print_array(c,CSIZE);
		
	return 0;
}


