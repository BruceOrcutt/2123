// defines maximum line length from standard input
#define MAXLINE 500

// standard libraries
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// custom libraries
#include "Graph.h"

// prototypes
void testDriver();
void getInput();
int  getNumber();
void getEdges();
void getSearches();

/**********************************************************************
csz860Project6 by Bruce A. Orcutt

Purpose:
    To create a graph, then find the shortest path

Command Parameters:
    None

Input:
    STDIN is expected.
    1st line is the number of verticies
    2nd line is the number of edges
    The edges then appear as 2 digit pairs, one per line
    A single digit states how many searches to perfomr
    The text ShortestPath: and two numbers inditcate each search to perform, one per line

Results:
    The shortest path for each pair of Vertices, or a statement that there is no such path

Returns:
    0: All is fine.

Notes:
    Does not perform well in snow.
**********************************************************************/


int main()
{
	char    szData[MAXLINE];    // line of input
    int     iSize     = 0;      // number of verticies
    int     iEdges    = 0;      // number of edges
    int     iSearches = 0;      // number of searches to perform
    Graph   pGraph;             // Our graph


    // get the number of verticies
    getInput(szData);
    iSize = getNumber(szData);

    if(iSize<1)
    {
        printf("ERROR: No valid number of verticies\n");
        return(-1);
    }

    // Create our graph!
    pGraph = newGraph(iSize);

    if(pGraph == NULL)
    {
        printf("ERROR: Unable to create graph\n");
        return(-2);
    }

    // get the edges
    getInput(szData);
    iEdges = getNumber(szData);

    getEdges(pGraph,iEdges);

    // get the searches
    getInput(szData);
    iSearches = getNumber(szData);

    printf("\n\nSearches\n========\n");
    getSearches(pGraph,iSearches);

    // clean up
    freeGraph(pGraph);

    return 0;
}

/******************** getEdges **********************************
void getEdges(Graph g,int n)

Purpose:
    Read the edges of the graph, via stdin

Parameters:
    I/O    Graph    g               The graph to add to
    I      int      n               The number of edges

Notes:
    Reads from STDIN via getInput()

Return value:
   NONE

**************************************************************************/
void getEdges(Graph g,int n)
{
    char    szLine[MAXLINE];
    int     i;  // loop index
    Edge    e;  // the edges to read, and add

    // get each edge, one by one
    for(i=0;i<n;i++)
    {
        // read the edge
        getInput(szLine);
        sscanf(szLine,"%d %d",&e.fromVertex,&e.toVertex);
        addEdge(g,e);
    }
} // end getEdges


/******************** getNumber **********************************
int getNumber(char *s)

Purpose:
    Given a string, get a number

Parameters:
    I   char pointer    s   String to parse

Notes:
    Are we there yet?

Return value:
   -1:  ERROR
   >0:  the read number

**************************************************************************/
// given a string, get an int
int getNumber(char *s)
{
    int iSize = 0;

    // we get nothing? ERROR!
    if(s == NULL)
    {
        printf("ERROR: Invalid number");
        return(-1);
    }

    // grab an int
    sscanf(s,"%d",&iSize);

    if(iSize < 1)
    {
        printf("ERROR: Invalid number");
        return(-1);
    }


} // end getNumber

/******************** getNumber **********************************
int getNumber(char *s)

Purpose:
    Given STDIN, get a line, return a string

Parameters:
    O   char pointer    szLine   String to send back

Notes:
    Almost there

Return value:
   NONE

**************************************************************************/
// put stdin string into szLine, up to MAXLINE
void getInput(char *szLine)
{

    if (fgets(szLine,MAXLINE,stdin) == NULL) return;
    if (szLine == NULL) return;

    // Chomp the \n, if it exists
    if(szLine[strlen(szLine)-1] == '\n')
        szLine[strlen(szLine)-1] = '\0';

    return;

} // end getInput


/******************** getSearches**********************************
void getSearches(Graph g,int n)

Purpose:
   Read the searches, and run them

Parameters:
    I   Graph   g   The graph we are searching
    I   int     n   The number of searches being provided

Notes:
    The meat

Return value:
   NONE

**************************************************************************/
void getSearches(Graph g,int n)
{
    int i;                      // loop index
    Vertex iStart,iDestination; // endpoints of searches
    char   szLine[MAXLINE];

    for(i=0;i<n;i++)
    {
        // initialization
        iStart       = 0;
        iDestination = 0;

        // read what to search
        getInput(szLine);
        sscanf(szLine+13,"%d %d",&iStart,&iDestination); // 13 is length of ShortestPath:

        // search
        shortestPath(g,iStart,iDestination);

    } // end for
} // end of getSearches