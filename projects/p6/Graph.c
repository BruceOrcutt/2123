//
// Created by borcutt on 12/1/17.
//
#include "Graph.h"

/******************** newGraph **********************************
Graph newGraph(int n)

Purpose:
    Given the number of vertices of the graph, malloc the nxn adjacency matrix and
    initialize every edge to 0 (i.e. the edges aren't there initially).
    Return the address of the graph.

Parameters:
   I    int n   Number of verticies to create, Integer.

Notes:
    Uses two sets of mallocs, one for the initial array, then another, looped for columns.

Return value:
    THe initialized graph.
**************************************************************************/
//Given the number of vertices of the graph, malloc the nxn adjacency matrix and initialize every edge to 0 (i.e. the edges aren't there initially).
//Return the address of the graph.
Graph newGraph(int n)
{
    Graph pGraph = NULL;   // to allocate and return
    int   i,j;      // index for loops for matrix initialization

    if (n<1) return NULL;  // A no vertex graph is nonsensical

    // allocate the control node
    pGraph = malloc(sizeof(GraphImp));

    // initialize
    pGraph->n = n;
    pGraph->adjacencyM =  NULL;

    pGraph->adjacencyM = (int **)malloc(n * sizeof(int **));

    for(i=0;i<n;i++)
        pGraph->adjacencyM[i] = (int *) malloc(n * sizeof(int *));

    // initialize the matrix
    for (i=0;i<n;i++)
        for(j=0;j<n;j++)
            pGraph->adjacencyM[i][j] = 0;

    // hand over what we created
    return(pGraph);

} // end newGraph

/******************** freeGraph **********************************
void freeGraph(Graph g)

Purpose:
   Graph Deconstructor. Free the adjacency matrix and then the graph itself.

Parameters:
   I/O    Graph g   The graph to deconstruct..

Notes:
    Two free sets used in the natrix, to grab the main and the columns, reversing the create

Return value:
   NONE.
**************************************************************************/

void freeGraph(Graph g) {
    int i;  // for loop index

    // do we even have a graph?
    if (g == NULL) return;

    // free each row
    for (i = 0; i < g->n; i++)
        free(g->adjacencyM[i]);

    // now free the rest of the matrix
    free(g->adjacencyM);

    // now, free what's left
    free(g);

    return;

} // end freeGraph

/******************** addEdge **********************************
void addEdge(Graph g, Edge e)

Purpose:
   Add the edge e to the graph g.

Parameters:
    I/O     Graph   g   The graph to add to
    I       Edge    e   The edge to add

Notes:
    Exits if one of the Vertexes is out of range

Return value:
   NONE.
**************************************************************************/
void addEdge(Graph g, Edge e)
{
    // Verify the vertex exists within the graph
    if(e.fromVertex > g->n || e.toVertex > g->n) return;

    // mark the edge as existing
    g->adjacencyM[e.fromVertex][e.toVertex] = TRUE;

} // end addEdge

/******************** firstAdjacent **********************************
Edge firstAdjacent(Graph g, Vertex v)

Purpose:
    Given graph g and vertex v, scan the adjacency matrix and return
    the first edge in g such that v is the "fromVertex" of the edge.

Parameters:
    I      Graph    g    The graph to add to
    I      Vertex   v   The vertex to start at

Notes:
    It's turtles, all the way down

Return value:
   Success: The relevant edge
   Failure: An edge with -1,-1
**************************************************************************/
Edge firstAdjacent(Graph g, Vertex v)
{
    int     i;  // loop index
    Edge    e;  // edge to return

    // scan the matrix, look for a match, if we find one, return it
    for(i=0;i<g->n;i++)
    {
       if(g->adjacencyM[v][i] == TRUE)
       {
           e.fromVertex = v;
           e.toVertex   = i;
           return e;
       } // end if
    } // end for

    // Default condition means none found, thus -1,-1
    e.toVertex      = -1;
    e.fromVertex    = -1;
    return e;
} // end firstAdjacent

/******************** nextAdjacent **********************************
Edge nextAdjacent(Graph g, Vertex v)

Purpose:
    Given graph g and vertex v, scan the adjacency matrix and return
    the next edge after e in g such that e.fromVertex is the fromVertex
    of the edge

Parameters:
    I      Graph    g   The graph to add to
    I      Edge     e   The edge to start at

Notes:
    Or is it elephants?

Return value:
   Success: The relevant edge
   Failure: An edge with -1,-1
**************************************************************************/
Edge nextAdjacent(Graph g, Edge e)
{
    int     i;  // for loop index
    Edge    e2;  // Edge to return

    // scan the matrix, look for a match
    for(i=e.toVertex+1;i<g->n;i++)
    {
        if(g->adjacencyM[e.fromVertex][i] == TRUE)
        {
            e2.fromVertex = e.fromVertex;
            e2.toVertex   = i;
            return e2;
        } // end if
    } // end for

    // NULL not allowed, as it's a struct, so returning 0,0
    e2.toVertex      = -1;
    e2.fromVertex    = -1;
    return e2;
} // end nextAdjacent


/******************** shortestPath **********************************
void shortestPath(Graph g, Vertex start, Vertex destination)

Purpose:
    Print the sequence of vertices on a shortest path in g starting from
    start and ending at destination.

    A shortest path should be computed using the Breadth First Search (BFS)
    algorithm that maintains the parents of each vertex in the shortest path
    tree as defined in class.

    BFS can be implemented directly in this function, or you may create a new function for BFS.

Parameters:
    I      Graph    g               The graph to add to
    I      Vertex   start           The Vertex to start at
    I      Vertex   destination     The Vertex to end at

Notes:
    BFS self contained within

Return value:
   NONE

**************************************************************************/
void shortestPath(Graph g, Vertex start, Vertex destination)
{
    Edge e;             // for parsing through edges
    Vertex curV, tempV; // placeholder verticies
    Queue q;            // to help with processing
    int i;              // for loop index
    int *iResultM;      // for reversing the parent list
    int *iVisitedM;     // avoiding cycles
    int *iParentM;      // verticies and their parents
    int count;          // How long is the path?

    // allocate it all
    q = newQueue();
    iResultM = (int *) malloc(sizeof(int) * g->n);
    iVisitedM = (int *) malloc(sizeof(int) * g->n);
    iParentM = (int *) malloc(sizeof(int) * g->n);

    // initialization
    insertQ(q, start);
    for (i = 0; i < g->n; i++)
    {
        iVisitedM[i] = FALSE;
        iParentM[i] = -1;
        iResultM[i] = -1;
    } // end for

    // Search
    while (removeQ(q, &curV)) {
        for (e = firstAdjacent(g, curV); e.toVertex != -1; e = nextAdjacent(g, e))
        {
            tempV = e.toVertex;

            if (!iVisitedM[tempV])
            {
                iVisitedM[tempV] = TRUE;
                iParentM[tempV] = curV;
                insertQ(q, tempV);
            } // end if
        } // end for
    } // end while

    // check if no path was found
    if(iParentM[destination]==-1)
    {
        printf("\nNo such path exists between %d and %d\n",start,destination);
        freeQueue(q);
        free(iParentM);
        free(iResultM);
        free(iVisitedM);
        return;
    } // end if

    // reverse the parents list
    count = 0;
    for (i = destination; i != start; i = iParentM[i])
    {
        iResultM[count] = iParentM[i];
        count++;
    }

    printf("\n");

    // Now print the path
    printf("Shortest path from %d to %d is: ",start,destination);
    for(i=count-1;i>=0;i--)
        printf("%d ",iResultM[i]);

    printf("%d\n",destination);

    // clean up
    freeQueue(q);
    free(iParentM);
    free(iResultM);
    free(iVisitedM);

} // end shortestPath