	/************************************************************************
Queue.h

Purpose:
    Struct definitions for a Queue.
    Define function prototypes used by Queues.
	
	This implementation of a Queue will store each value as a Linked List, with the oldest element in the list at the head of the list and the newest element in the list at the foot of the list.
************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stdlib.h>


//#define constant values
#define TRUE 1
#define FALSE 0

//typedef for the Vertex which is simply an integer to store the ID of a vertex in a graph in this project.
typedef int Vertex;


//Typedef for a node in the queue.
typedef struct NodeQ
{
    Vertex v;
    struct NodeQ *pNext;
	
} NodeQ;

//Typedef for a Queue implementation.  
//pHead points to the node waiting in the Queue the longest (first to be removed).
//pFoot points to the node waiting in the Queue the shortest (most recently added node).
typedef struct
{
    NodeQ *pHead;
	NodeQ *pFoot;
} QueueImp;

typedef QueueImp *Queue;


/*****Prototypes*******/

//Malloc a new QueueImp and return it's address.
Queue newQueue();


//Free the Queue and any nodes that still exist.
void freeQueue(Queue q);


//Allocate a new node and store "v" as the vertex in the node.  Return the address of the node.
NodeQ *allocateNodeQ(Vertex v);


//Create a node to store the given Element and add it to the end of the Queue.
void insertQ(Queue q, Vertex v);

//Remove the vertex at pHead in q and return it via *v (passed by reference).
//Functionally return FALSE if the queue is empty and return TRUE otherwise.
int removeQ(Queue q, Vertex *v);
