//
// Created by borcutt on 12/1/17.
//
#include "Queue.h"

/******************** newQueue **********************************
Queue newQueue()

Purpose:
    Malloc a new QueueImp and return it's address.

 Parameters:
    NONE

Notes:
    Certain portions may have been writen by my cat.

Return value:
   The newly allocated and initialized queue

**************************************************************************/
Queue newQueue()
{
    Queue pQueue;   // a queue to return

    // allocate the space for the two pointers
    pQueue = (Queue)malloc(sizeof(QueueImp));

    // initialize the starting pointers to NULL
    pQueue->pFoot = NULL;
    pQueue->pHead = NULL;

    return (pQueue);
} // end newQueue

/******************** freeQueue **********************************
void freeQueue(Queue q)

Purpose:
    Free the Queue and any nodes that still exist.

 Parameters:
    I/O Queue   q   THe queue to delete

Notes:
    On NULL, ignore.

Return value:
   The newly allocated and initialized queue

**************************************************************************/
void freeQueue(Queue q)
{
    NodeQ *pCur,*pTemp;    // for traversing

    // if passed NULL, nothing to do
    if(q == NULL) return;

    // otherwise, we have nodes to clean up
    pCur = q->pHead;
    while(pCur != NULL)
    {
        pTemp = pCur->pNext;
        free(pCur);
        pCur = pTemp;
    } // end while
    free(q);
} // end freeQueue

/******************** allocateNodeQ **********************************
NodeQ *allocateNodeQ(Vertex v)

Purpose:
   Allocate a new node and store "v" as the vertex in the node.
   Return the address of the node

 Parameters:
    I   Vertex  v   The vertex value for the queue node we're adding

Notes:
    Loooooow Lands... Low lands awaaaaay

Return value:
   The newly allocated node

**************************************************************************/
NodeQ *allocateNodeQ(Vertex v)
{
    NodeQ *pNode; // new node to create

    // allocate
    pNode = malloc(sizeof(NodeQ));

    // initialize
    pNode->pNext = NULL;
    pNode->v = v;

    // profit
    return(pNode);
} // end allocateNodeQ

/******************** insertQ **********************************
void insertQ(Queue q, Vertex v)

Purpose:
    Create a node to store the given Element and add it to the end of the Queue.

 Parameters:
    I/O Queue   q   The queue to add to
    I   Vertex  v   The vertex value for the queue node we're adding

Notes:
    Are you even reading these?

Return value:
   The newly allocated node

**************************************************************************/
void insertQ(Queue q, Vertex v)
{
    NodeQ *pNode;  // new node we will create

    // Lets be generous.  If passed in no queue, will create it
    if (q == NULL) q = newQueue();

    // get it malloced
    pNode = allocateNodeQ(v);

    // check for an empty queue
    if (q->pHead == NULL)
    {
        // if so,new node is head and foot
        q->pHead = pNode;
        q->pFoot = pNode;
    }
    else  // otherwise, we just need to add the node after the foot.
    {
        q->pFoot->pNext = pNode;  // put it after current foot
        q->pFoot = pNode;         // now reset the foot to the new locale
    }
} // end insertQ


/******************** removeQ **********************************
int removeQ(Queue q, Vertex v)

Purpose:
    Remove the vertex at pHead in q and return it via *v (passed by reference)

 Parameters:
    I/O Queue           q   The queue to remove from
    I   Vertex pointer  v   The vertex value for the queue node we're removing, used for passing back value

Notes:
    Value is being passed back via *v

Return value:
   FALSE:   Empty queue
   TRUE:    Success

**************************************************************************/
int removeQ(Queue q, Vertex *v)
{
    NodeQ *pTemp; // hold old node while we move.

    pTemp = q->pHead;

    // if null, error
    if(pTemp == NULL) return(FALSE);

    // do we have existing nodes or not
    if(pTemp->pNext != NULL)
    {

        pTemp = q->pHead->pNext;
        *v = pTemp->v;
        free(q->pHead);
        q->pHead = pTemp;

    }
    else
    {
        *v = pTemp->v;
        free(q->pHead);
        q->pHead = NULL;
        q->pFoot = NULL;

    }

    return(TRUE);
} // end removeQ
