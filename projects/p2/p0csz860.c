
/******************************************************************************
cs2123p0.c by Bruce Orcutt
Purpose:
    This program reads courses and their attributes.  It displays that information.
Command Parameters:
    n/a
Input:
    The standard input file stream contains two types of records (terminated
    by EOF).  COURSE records are followed by zero to many ATTR records
    (terminated by either EOF or another COURSE record).
    COURSE szCourseId szName
      8s           7s        20s (but the name may have blanks)
    ATTR    szAttrType  szAttrValue
      8s          10s        12s
Results:
    For each course, it prints the course ID, name, and the attributes.
Returns:
    0 - normal
    1 - too many courses
    2 - too many attributes
    3 - bad input data
Notes:
    1. This program only allows for 30 courses.
    2. A course may have multiple occurrences of the same attribute type.  For example,
       a course may have many PREREQ.
    3. The number of attributes for a course must not exceed 12.
*******************************************************************************/
// If compiling using visual studio, tell the compiler not to give its warnings
// about the safety of scanf and printf
#define _CRT_SECURE_NO_WARNINGS 1

// Include files

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "cs2123p2.h"



/******************** printCourseData **********************************
void printCourseData(Course courseM[], int iNumCourse)

Purpose:
	Provide a structured printing of entered course data and attribute values

Parameters:
    	I Course courseM[]	:	arrary containing all course data entered by getCourseData()
	I int iNumCourse 	:	size of the courseM array	

Notes:
	Assumes all valid data, due to data validation within getCourseData. 
	

Return value:
	None

**************************************************************************/

void printCourseData(Course courseM[], int iNumCourse)
{
    	int i;
    	int j;
    	
	// Print a heading for the list of courses and attributes
	printf("ID\tCourse Name\n");
        printf("\t\t\tAttr\tValue\n" );

    	for (i = 0; i < iNumCourse; i++)
    	{	

        	// Print a course Id and Name
		printf("%s\t%s\n",courseM[i].szCourseId,courseM[i].szCourseName);
			
        	//  Print each attribute
        	for (j = 0; j < courseM[i].iNumberOfAttrs; j++)
        	{
        		printf("\t\t\t%s\t%s\n",courseM[i].attrM[j].szAttrType,courseM[i].attrM[j].szAttrValue);;
		}
		printf("\n");
    	}
}

