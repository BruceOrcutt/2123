#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "cs2123p2.h"



/******************** evaluatePostfix **************************************
void evaluatePostfix(PostfixOut postfixOut, Course courseM[], int iNumCourse, QueryResult resultM[])
Purpose:
    Based on the passed in query in postfixOut, mark in QueryResult TRUE for each course
    in courseM that meets the requirements.
Parameters:
    I PostfixOut    postfixOut      The query, in postfix
    I Course        courseM[]       Array of all courses.
    I int           iNumCourse      Size of the courseM array
    O QueryResult   resultM[]       TRUE or FALSE, based on each query evaluation
Notes:
    requires never(), only(), equals()
Return value:
    none, void.

**************************************************************************/
void evaluatePostfix(PostfixOut postfixOut, Course courseM[], int iNumCourse, QueryResult resultM[]) {
    int     i,j,k;                  // for loop indexes
    Stack   evaluationStack;        // stack for evaluating the postfix
    Stack   booleanStack;           // stack for evaluating the postfix results (AND, OR)
    Element curQueryItem;           // current postfix element to evaluate
    Attr    tempAttr;               // for use in eval functions
    Element boolItem,boolItem2;     // for AND, OR

    // create a new stack, used to evaluate the query.
    evaluationStack = newStack();
    booleanStack    = newStack();

    // loop over all courses
    for (j = 0; j <= iNumCourse; j++)
    {
        // Loop over all postfix items in the query
        for (i = 0; i < postfixOut->iPostfixOutCount; i++)
        {
            // get the next item of the postfix string
            curQueryItem = postfixOut->postfixOutM[i];

            // check next item in postfix
            switch (curQueryItem.iCategory) {
                // is it an operand?
                case CAT_OPERAND:
                    // is it an operand? add to the stack
                    push(evaluationStack, curQueryItem);
                break;  // end OPERAND

                // is it an operator?
                case CAT_OPERATOR:
                    // take action based on operator type

                    // NEVER
                    if (strcmp(curQueryItem.szToken, "NEVER") == 0)
                    {
                        // we'll operate on the last to valid values
                        strcpy(tempAttr.szAttrValue, pop(evaluationStack).szToken);
                        strcpy(tempAttr.szAttrType,  pop(evaluationStack).szToken);

                        // see if this course matches
                        boolItem.bInclude = never(&courseM[j], tempAttr);

                        // add the string to match the bInclude
                        if (boolItem.bInclude == TRUE)
                            strcpy(boolItem.szBoolean, "TRUE");
                        else
                            strcpy(boolItem.szBoolean, "FALSE");

                        // add to the boolean stack
                        push(booleanStack, boolItem);
                    } // end of NEVER

                    // ONLY
                    if (strcmp(curQueryItem.szToken, "ONLY") == 0)
                    {
                        // we'll operate on the last to valid values
                        strcpy(tempAttr.szAttrValue, pop(evaluationStack).szToken);
                        strcpy(tempAttr.szAttrType,  pop(evaluationStack).szToken);

                        // see if this course matches
                        boolItem.bInclude = only(&courseM[j], tempAttr);

                        // add the string to match the bInclude
                        if (boolItem.bInclude == TRUE)
                            strcpy(boolItem.szBoolean, "TRUE");
                        else
                            strcpy(boolItem.szBoolean, "FALSE");

                        // add to the boolean stack
                        push(booleanStack, boolItem);
                    } // end of ONLY

                    // EQUALITY
                    if (strcmp(curQueryItem.szToken, "=") == 0)
                    {
                        // we'll operate on the last to valid values
                        strcpy(tempAttr.szAttrValue, pop(evaluationStack).szToken);
                        strcpy(tempAttr.szAttrType,  pop(evaluationStack).szToken);

                        // see if this course matches
                        boolItem.bInclude = equals(&courseM[j],tempAttr);

                        // add the string to match the include
                        if (boolItem.bInclude == TRUE)
                            strcpy(boolItem.szBoolean, "TRUE");
                        else
                            strcpy(boolItem.szBoolean, "FALSE");

                        // add to the boolean stack
                        push(booleanStack, boolItem);

                    } // end of EQUALITY

                    // AND
                    if (strcmp(curQueryItem.szToken, "AND") == 0)
                    {
                        // boolean stack used if AND (prior results from other operators being compared)
                        boolItem  = pop(booleanStack);
                        boolItem2 = pop(booleanStack);

                        // check items with AND
                        if((boolItem.bInclude == TRUE) && (boolItem2.bInclude == TRUE))
                        {
                            // if both TRUE, push TRUE on stack
                            // as it's already a TRUE. string already set
                            push(booleanStack,boolItem);
                        }
                        else
                        {
                         // otherwise we push a FALSE on the stack
                            boolItem.bInclude = FALSE;
                            strcpy(boolItem.szBoolean,"FALSE");
                            push(booleanStack,boolItem);
                        }
                    } // end of AND

                    // OR
                    if (strcmp(curQueryItem.szToken, "OR") == 0)
                    {
                        // boolean stack used if OR (prior results from other operators being compared)
                        boolItem  = pop(booleanStack);
                        boolItem2 = pop(booleanStack);

                        // check items with OR
                        if((boolItem.bInclude == TRUE) || (boolItem2.bInclude == TRUE))
                        {
                            // as either could be true, force item for stack to be true.
                            boolItem.bInclude = TRUE;
                            strcpy(boolItem.szBoolean,"TRUE");
                            push(booleanStack,boolItem);
                        }
                        else
                        {
                            // otherwise we push a false on the stack
                            boolItem.bInclude = FALSE;
                            strcpy(boolItem.szBoolean,"FALSE");
                            push(booleanStack,boolItem);
                        }
                    } // end of OR

                break;  // OPERATOR

                default:
                    ErrExit(ERR_BAD_INPUT, "Error, invalid item in outfix stack");
            } // end switch
        } // end query loop

        // FINAL RESULT
        // last value in boolean stack will be our final result
        boolItem   = pop(booleanStack);
        // store the result in the proper slot for this course
        resultM[j] = boolItem.bInclude;
    } // end course loop

    // free the stacks
    freeStack(evaluationStack);
    freeStack(booleanStack);

} // end evaluatePostfix

/******************** atLeastOne **************************************
int atLeastOne(Course *pCourse, Attr attr)
Purpose:
    Determines whether a course has a particular attribute (type and
    value). If it does, returns TRUE.
Parameters:
    I Course *pCourse     One course structure which also
                             contains attributes of that course.
    I Attr    attr        The attribute that we don't want this
                             course to have.
Notes:
    While implemented, not actually used by other code (unneeded)
Return value:
    FALSE - course didn't have the specified attribute exclusively
    TRUE  - course did have it
**************************************************************************/
int atLeastOne(Course *pCourse, Attr attr)
{
    int i = 0;  // for loop index

    // Make certain the course information was passed to this function.
    if (pCourse == NULL)
        ErrExit(ERR_ALGORITHM, "function only() received a NULL pointer");

    // Loop through the attributes to see if the specified
    // attribute type and value exists.
    for (i = 0; i < (pCourse->iNumberOfAttrs); i++)
    {
        // Does this item match?
        if ((strcmp(pCourse->attrM[i].szAttrType, attr.szAttrType) == 0)
            && (strcmp(pCourse->attrM[i].szAttrValue, attr.szAttrValue) == 0))
        {
            //  first match, set to true
            return TRUE;
        }
    } // end for

    // if we get here, no matches, so false as not one
    return FALSE;  // EXIT POINT
} // end atLeastOne

/******************** only **************************************
int only(Course *pCourse, Attr attr)
Purpose:
    Determines whether a course has a particular attribute (type and
    value) and no other value. If it contains the correct value, but
    no other values for the type, it reutrns true.
Parameters:
    I Course *pCourse     One course structure which also
                             contains attributes of that course.
    I Attr    attr        The attribute that we don't want this
                             course to have.
Notes:
    Used in evaluatePostfix
Return value:
    FALSE - course didn't have the specified attribute exclusively
    TRUE  - course did have it
**************************************************************************/
int only(Course *pCourse, Attr attr)
{
    int i = 0;                  // index for for loop
    int bReturnValue = FALSE;   // did we find an exact match?
    Attr tempAttr;              // holds one value to compare in loop, makes compare statement cleaner

    // Make certain the course information was passed to this function.
    if (pCourse == NULL)
        ErrExit(ERR_ALGORITHM, "function only() received a NULL pointer");

    // Loop through the attributes to see if the specified
    // attribute type and value exists.  If so, only will be TRUE, if it's exclusive
    for (i = 0; i < (pCourse->iNumberOfAttrs); i++)
    {
        // copy the attribute to compare
        strcpy(tempAttr.szAttrType,pCourse->attrM[i].szAttrType);
        strcpy(tempAttr.szAttrValue,pCourse->attrM[i].szAttrValue);

        // Does this item match exactly? Then set return value
        if ((strcmp(tempAttr.szAttrValue,attr.szAttrValue) == 0) &&
            (strcmp(tempAttr.szAttrType,attr.szAttrType) == 0))
                bReturnValue = TRUE;
        // otherwise, if we have any other value for the type, it's no longer exclusive!
        else if (strcmp(tempAttr.szAttrType,attr.szAttrType) == 0)
            return FALSE;   // EXIT POINT!
     } // end for

    return bReturnValue;  // EXIT POINT

} // end only function

/******************** equals **************************************
int equals(Course *pCourse, Attr attr)
Purpose:
    Determines whether a course has a particular attribute (type and
    value) and no other value. If it contains the correct value, reutrns
    TRUE.
Parameters:
    I Course *pCourse     One course structure which also
                             contains attributes of that course.
    I Attr    attr        The attribute that we don't want this
                             course to have.
Notes:
    Used in evaluatePostfix
Return value:
    FALSE - course didn't have the specified attribute exclusively
    TRUE  - course did have it
**************************************************************************/
int equals(Course *pCourse, Attr attr)
{
    int i = 0;                  // for loop index
    int bReturnValue = FALSE;   // return value

    // Make certain the course information was passed to this function.
    if (pCourse == NULL)
        ErrExit(ERR_ALGORITHM, "function only() received a NULL pointer");

    // Loop through the attributes to see if the specified
    // attribute type and value exists.  If so, only will be TRUE, if it's exclusive
    for (i = 0; i < (pCourse->iNumberOfAttrs); i++)
    {
        // Does this item match?
        if (strcmp(pCourse->attrM[i].szAttrType, attr.szAttrType) == 0
            && strcmp(pCourse->attrM[i].szAttrValue, attr.szAttrValue) == 0)
        {
            // Any match means true
            bReturnValue = TRUE;
        }
    } // end for

    return bReturnValue;  // EXIT POINT
} // end equals

