#include <stdio.h>
#include <string.h>
#include <printf.h>
#include "cs2123p2.h"


/******************** process_operators **********************************
int process_operators(Stack opStackM, PostfixOut postfixOut, Element curElement)
Purpose:
    Given an operator, add it to the output, or find it's correct spot.
Parameters:
    I/O   opStackM    The stack of operators
    I/O   postfixOut  The string, converted to postfix notation
    I     curElement  The current operator to process.
Notes:
   Operators:   =,ONLY, NEVER, AND, OR
   Precidence:  (high to low)
                ()
                =, NEVER, ONLY
                AND, OR
Return value:
   Defined in c22123p1.h (WARN continues to the next, ERR exits immediately)
        WARN_MISSING_RPAREN     missing right paren
        WARN_MISSING_LPAREN     missing left paren
        WARN_MISSING_OPERATOR   operator missing
        WARN_MISSING_OPERAND    operand missing
        ERR_STACK_USAGE         Issue with the stack
        ERR_OUT_OVERFLOW        Out string too long
        ERR_ALGORITHM           Issue with the Algorithm
        0                       Success!
**************************************************************************/

int process_operators(Stack opStackM, PostfixOut postfixOut, Element curElement)
{
    int bDone = FALSE;  // flag for checking status of loop

    // For operators, must scroll through stack, and check precidence
    while(!bDone)
    {

        // no other operator, so add to the op stack
        if(isEmpty(opStackM))
        {
            push(opStackM,curElement);
            bDone = TRUE;
        }
        // else if precidence lesser than new, push
        else if(curElement.iPrecedence <= topElement(opStackM).iPrecedence)
            addPostfixOut(postfixOut,pop(opStackM));
        // otherwise push, and continue until empty or greater precidence found
        else
        {
            push(opStackM,curElement);
            bDone = TRUE;
        }
    } // end, operator while

    return 0;
} // end process_operators

/******************** convertToPostfix **********************************
int convertToPostfix(char *pszInfix, PostfixOut postfixOut)
Purpose:
    Given a string, in Infix notation, provide the appropriate Postfix notation.
Parameters:
    I   pszInfix    The infix string to be converted
    O   postfixOut  The string, converted to postfix notation
Notes:
   Operators:   =,ONLY, NEVER, AND, OR
   Precidence:  (high to low)
                ()
                =, NEVER, ONLY
                AND, OR
Return value:
   Defined in c22123p1.h (WARN continues to the next, ERR exits immediately)
        WARN_MISSING_RPAREN     missing right paren
        WARN_MISSING_LPAREN     missing left paren
        WARN_MISSING_OPERATOR   operator missing
        WARN_MISSING_OPERAND    operand missing
        ERR_STACK_USAGE         Issue with the stack
        ERR_OUT_OVERFLOW        Out string too long
        ERR_ALGORITHM           Issue with the Algorithm
        0                       Success!
**************************************************************************/
int convertToPostfix(char *pszInfix, PostfixOut postfixOut)
{
    Stack   opStackM;                   // stack to hold all operators we encounter
    char    szToken[MAX_TOKEN];         // holds the tokens we'll read
    char    *szModifiedInput;           // Takes the input, but value modified to allow parsing
    Element curElement;                 // for categorizing

    // FLAGS
    int     bDone       =   FALSE;      // Flag to check for end of our loop
    int     bOperator   =   FALSE;      // Was last item an operator
    int     bOperand    =   FALSE;      // was last item an operand
    int     bLParen     =   FALSE;      // was last item a left paren
    int     iParenCount =   0;          // checks for matching number of parens

    // make sure we have data to process, if none, error
    if(strlen(pszInfix) == 0) return WARN_MISSING_OPERAND;    // EXIT POINT

    // allocate the stack to hold operators
    opStackM  = newStack();

    // local copy of input provided, to ensure we aren't destructive, just in case.
    szModifiedInput = pszInfix;

    // parase through all the tokens in the input string.  Exit with error code if issue with input
    while(!bDone)
    {
        // check to see if we have mismatch parens
        if(iParenCount < 0) return WARN_MISSING_LPAREN;

        // get the current token
        szModifiedInput = getToken(szModifiedInput,szToken,MAX_TOKEN-1);

        // is the input string empty?
        if (szModifiedInput == NULL)
        {
            bDone = TRUE;   // flag to end the outer loop
            break;          // we're done, end the loop
        }

        // populate the element to process, and initialize the properties
        strcpy(curElement.szToken,szToken);
        curElement.iPrecedence = 0;
        curElement.iCategory = 0;

        // categorize the token into operand or operator
        categorize(&curElement);

        // based on the categorization, now we act!
        switch(curElement.iCategory) {
            // if an operand, add to the output
            case CAT_OPERAND: // OPERAND
                // if two operands in a row, we're missing an operator
                if (bOperand) return WARN_MISSING_OPERATOR;   //EXIT POINT
                else
                {
                    // Valid! So set the flags and add the new element to the postfix out.
                    bOperand = TRUE;
                    bOperator = FALSE;
                    bLParen = FALSE;
                    addPostfixOut(postfixOut, curElement);
                }
            break; // End OPERAND

                // if a left paren, then add to the operand stack
            case CAT_LPAREN:  // (
                // a left paren MUST be preceded by an operator, if not, it's an error!
                if (bOperand) return WARN_MISSING_OPERATOR;    //EXIT POINT
                else
                {
                    // Valid Left paren! Set the flag, push it, and add it to the count of parens.
                    bLParen = TRUE;
                    push(opStackM, curElement);
                    iParenCount++;
                }
            break;  // End LPAREN

                // if a right paren, then search for the left paren!
            case CAT_RPAREN:  // )
                // inside a set of parens, the last item MUST NOT be an operator.
                if (bOperator) return WARN_MISSING_OPERAND;    //EXIT POINT
                // if RPARENT right after LPARENT, invalid, need operand inside.
                else if (bLParen) return WARN_MISSING_OPERAND; //EXIT POINT
                // Valid right Paren, so far.
                else
                {
                    // first, reset the flags, and remove one from the LPAREN count.
                    bOperand = FALSE;
                    bOperator = FALSE;
                    iParenCount--;

                    // pop until we find the matching paren, or we run out of operators stored up.
                    while ((!isEmpty(opStackM)) && (topElement(opStackM).iCategory != CAT_LPAREN))
                        addPostfixOut(postfixOut, pop(opStackM));

                    // did we find a LPAREN? If not, ERROR!
                    if (isEmpty(opStackM)) return WARN_MISSING_LPAREN; //EXIT POINT

                    // Now that we found the matching paren, we're done. Now just need to dump the LPAREN
                    pop(opStackM);
                } // end of else
            break; // end RPARENT

            // We have an OPERATOR
            case CAT_OPERATOR:
                // Two operators in a row is invalid, exit in error
                if(bOperator) return WARN_MISSING_OPERAND;    //EXIT POINT
                else
                {
                    // We have a valid operator!

                    // Reset the flags to reflect having an operator
                    bLParen     = FALSE;
                    bOperand    = FALSE;
                    bOperator   = TRUE;

                    // loop through the operators, checking for precidence.
                    process_operators(opStackM, postfixOut, curElement);
                } // end else
            break; // end OPERATOR

            // We should NEVER get here.
            default:
                printf("Undefined condition with %s\n",curElement.szToken);
        } // end switch
    } // end while

    // Now at the end of the input, do our last bit of processing.

    // if the last item was an operator, that'd be an issue
    if(bOperator) return WARN_MISSING_OPERAND;    //EXIT POINT

    // unset the flags
    bOperand    = FALSE;
    bOperator   = FALSE;

    // time to empty the operator stack, and add them into the postfix
    while (!isEmpty(opStackM))
    {
        // if we get an LPAREN at this point, it has no RPAREN, so an error!
        if((topElement(opStackM).iCategory == CAT_LPAREN)) return WARN_MISSING_RPAREN;
        //Otherwise, valid, add to the out!
        addPostfixOut(postfixOut, pop(opStackM));
    }

    // LPAREN = +1 RPAREN = -1 so, negative means too many RPAREN, positive too many LPAREN
    if (iParenCount<0) return WARN_MISSING_LPAREN;
    else if (iParenCount>0) return WARN_MISSING_RPAREN;


    // if(bOperator) return WARN_MISSING_OPERAND;

    // clean up after ourselves
    freeStack(opStackM);

    return 0;
}


