#include "BinarySearchTree.h"

/**********************************************************************
p4 by Bruce A. Orcutt

Purpose:
    This program takes a series of commands, that:
        Insert numbers in a binary search tree
        Search for numbers in the tree
        Print the existing tree

Command Parameters:
    No command line Parameters, however the program expects it's commands
    via standard Input.

Input:
    All commands entered via standard input.

    The following are valid commands
        INSERT X:       Inserts X into the tree
        SEARCH X:       Looks for the value X in the tree
        PRINT INORDER:  Prints the tree, in "in order" format
        PRINT PREORDER: Prints the tree, in "pre order" format

Results:
    On INSERT:
            Valid:      Inserted X into the tree
            Invalid:    X is already in the tree
    On SEARCH:
            In:         Found X
            Out:        X not in tree
    On PRINT:
            INORDER:    Prints the nodes of the tree, in order, one per line
            PREORDER:   Prints the nodes of the tree, pre order, one per line

Returns:
    Always returns 0.

Notes:

**********************************************************************/
int main()
{
    int                 iValue;                 // For reading values to add
    char                szData[MAX_URL_LENGTH]; // Command to process
    BinarySearchTree    searchTree;             // Search tree to use
    Element             element;                // Value to insert

    // let's get our tree
    searchTree = newBinarySearchTree();

    // grab standard input, one line at a time
    while(fgets(szData,MAX_URL_LENGTH,stdin) != NULL)
    {
        // Chomp the \n, if it exists
        if(szData[strlen(szData)-1] == '\n')
            szData[strlen(szData)-1] = '\0';

        // INSERT command
        if(strstr(szData,"INSERT"))
        {
            // Read the value to insert
            sscanf(szData+6,"%d",&iValue);

            // set the element value
            element.key = iValue;

            // attempt to insert
            if(insert(searchTree,element) == FALSE)
                printf("%d already in the tree\n",iValue);
            else
                printf("Inserted %d into tree\n",iValue);
        } // end if INSERT

        // SEARCH command
        else if(strstr(szData,"SEARCH"))
        {
           // Read the value to search for
           sscanf(szData+6,"%d",&iValue);

           // attempt to insert
           if(search(searchTree->pRoot,iValue) != NULL)
               printf("Found %d\n",iValue);
           else
               printf("%d not in the tree\n",iValue);
        } // end if SEARCH

        // PRINT command
        else if(strstr(szData,"PRINT"))
        {
            if(strstr(szData,"INORDER"))
                printInOrder(searchTree->pRoot);
            else if(strstr(szData,"PREORDER"))
                printPreOrder(searchTree->pRoot);
            else
                printf("Unrecognized Print Command %s\n",szData);
        } // end if PRINT
        else printf("Unrecognized commad [%s]",szData);

    } // end while

    // time to chop it down
    freeBinarySearchTree(searchTree);

    return 0;
}


