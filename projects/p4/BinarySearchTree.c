#include "BinarySearchTree.h"

// Prototypes internal to functions of this file
//Free node, allows for freeing to be done recursively
void freeNode(NodeT *p);

// Recursively find the correct spot for a node, then add it
int insertNode(NodeT *p,NodeT *pNew);

/******************** newBinarySearchTree **********************************
 BinarySearchTree newBinarySearchTree()

Purpose:
    Allocates the space for a new binary search tree, and performs initialization

Parameters:
    NONE

Notes:
    Data structure is defined in BinarySearchTree.h

    Only the control struct is allocated.  No data nodes are allocated.

 Return value:
    BinarySearchTree    A pointer to the allocated control node.
**************************************************************************/
BinarySearchTree newBinarySearchTree()
{
    BinarySearchTree newTree;   // the new tree to allocate.

    newTree = (BinarySearchTree) malloc(sizeof(BinarySearchTreeImp));

    newTree->pRoot = NULL;

    return(newTree);
} // end of newBinrarySearchTree


/******************** freeBinarySearchTree **********************************
void freeBinarySearchTree(BinarySearchTree tree)

Purpose:
    Frees the binary search tree, and all included nodes

 Parameters:
    I/O tree    Tree and all contained nodes will be freed.

 Notes:
    Datastructure defined in BinarySearchTree.h

    Relies on the freeNode() function to actually free the associated nodes.

 Return value:
    NONE
**************************************************************************/
void freeBinarySearchTree(BinarySearchTree tree)
{

    // check to see if we have a tree at all
    if(tree == NULL)
        return;

    // Call the recrusive node freer
    freeNode(tree->pRoot);

    // Now that all sub nodes are freed, free the top
    free(tree);

} // end of freeBinarySearchTree

/******************** freeNode *********************************************
void freeNode(NodeT *p)

Purpose:
    Frees a node, and anything underneath, recursively

 Parameters:
    I/O p   A node along the tree to be freed.

 Notes:
    Datastructure defined in BinarySearchTree.h

    Called by freeBinarySearchTree()

 Return value:
    NONE
**************************************************************************/
void freeNode(NodeT *p)
{
    NodeT *pCur;

    // See if we have nothing to free
    if(p == NULL)
        return;

    // Free the left, the right, then the node itself
    freeNode(p->pLeft);
    freeNode(p->pRight);
    free(p);

} // end freeNode


/******************** allocateNode ****************************************
NodeT *allocateNode(Element value)

Purpose:
    Allocates a node for later insertion, with the passed in value.

 Parameters:
    I value A value to put into the Element portion of the node

 Notes:
    Datastructure defined in BinarySearchTree.h

    Called by insert()

    Does not insert itself

 Return value:
    A pointer to the newly allocated node, with Element set to value
**************************************************************************/
NodeT *allocateNode(Element value)
{
    NodeT *pNew;    // the node to allocate and return

    // Allocate the space
    pNew = (NodeT *)malloc(sizeof(NodeT));

    // set the value
    pNew->element = value;

    // Initialize the right and left pointers
    pNew->pRight = NULL;
    pNew->pLeft = NULL;

    return(pNew);
} // end allocateNode


/******************** search *********************************************
NodeT *search(NodeT *p, int searchValue)

Purpose:
    Recursive algorithm for searching for a node with key value equal
    to searchValue.  Return a pointer to the node if you find it or
    return NULL if it does not exist.

 Parameters:
    I p             A node in the tree, head of a subset of the tree, to search
    I searchValue   Value to search for

 Notes:
    Datastructure defined in BinarySearchTree.h

    Called by insert()

 Return value:
    FOUND:      Pointer to the node that contains the value
    NOT FOUND:  NULL
**************************************************************************/
NodeT *search(NodeT *p, int searchValue)
{
    // If we have a NULL, we finished our search, return NULL
    if(p == NULL)
        return(NULL);

    // If we have a node, we have 3 possible cases, equality, left or right.

    // Value found, return current p!
    if(p->element.key == searchValue)
        return(p);

    // if we don't have it, we continue to search, based on < or >
    if(searchValue < p->element.key)
        search(p->pLeft,searchValue);
    else
        search(p->pRight,searchValue);

} // end search


/******************** insert *********************************************
int insert(BinarySearchTree tree, Element value)

Purpose:
    Create a node to store the given Element and add it as a leaf in the
    BinarySearchTree.  No balancing of the tree takes place.

 Parameters:
    I/O tree    The tree that we'll add the node to.
    I   value   Value to add to the tree

 Notes:
    Datastructure defined in BinarySearchTree.h

    Called by insert()

 Return value:
    TRUE:   If the insert worked successfully,
    FALSE:  If the node already existed in the tree.
**************************************************************************/
int insert(BinarySearchTree tree, Element value)
{
    NodeT   *pNew;      // The new node to add

    // Allocate the new node
    pNew = allocateNode(value);

    // First, do we have an empty tree?
    // If so, just add, and set as root.
    if(tree->pRoot == NULL)
    {
        tree->pRoot = pNew;
        return(TRUE);
    } // end if, empty

    // Does the tree already have this value? If so, free the new one, and leave
    if(search(tree->pRoot,value.key) != NULL)
    {
        free(pNew);
        return(FALSE);
    } // end if, found

    // recursively find spot for node, and place it in
    insertNode(tree->pRoot,pNew);

} // end insert


/******************** insert *********************************************
int insertNode(NodeT *p,NodeT *pNew)

Purpose:
    Given a node in the tree, attempt to insert a new node, recursively.

 Parameters:
    I/O p       A node of the tree that we'll add the node to.
    I   pNew    Value to add to the tree

 Notes:
    Data structure defined in BinarySearchTree.h

    Called by insert()

    As insert() checks for NULL and equality, they are not checked here.

 Return value:
    TRUE:   If the insert worked successfully,
    FALSE:  If the node already existed in the tree.
**************************************************************************/
int insertNode(NodeT *p,NodeT *pNew)
{

    // if it's less, then on the left
    if(p->element.key > pNew->element.key)
    {
        // no left, then this is new left
        if(p->pLeft == NULL)
        {
            p->pLeft = pNew;
            return(TRUE);
        }
        else
            insertNode(p->pLeft,pNew);
    } // end if, left
    // Otherwise right
    else
    {
        // no right, then this is new right
        if(p->pRight == NULL)
        {
            p->pRight = pNew;
            return(TRUE);
        }
        else
            insertNode(p->pRight,pNew);
    } // end if, right

} // end insertNode

/******************** printInOrder *********************************************
void printInOrder(NodeT *p)

Purpose:
    Recursivly print the key values of all nodes in the subtree rooted at p
    in increasing order.

 Parameters:
    I   p       The current starting point of our tree printing

 Notes:
    Datastructure defined in BinarySearchTree.h

 Return value:
    NONE
**************************************************************************/
void printInOrder(NodeT *p)
{

    // In order uses the following order: left, self, right

    // if no node, we reached the bottum of this branch
    if(p == NULL)
        return;

    // left
    printInOrder(p->pLeft);

    // center
    printf("%d\n",p->element.key);

    // Right
    printInOrder(p->pRight);

} // end printInOrder


/******************** printPreOrder *********************************************
void printPreOrder(NodeT *p)

Purpose:
    Recursivly print the key values of all nodes in the subtree rooted at p
    according to a preorder traversal.

 Parameters:
    I   p       The current starting point of our tree printing

 Notes:
    Data structure defined in BinarySearchTree.h

 Return value:
    NONE
**************************************************************************/
void printPreOrder(NodeT *p)
{
    // For pre-order, the order is: self, left, right

    // if NULL, done.
    if(p == NULL)
        return;

    // center
    printf("%d\n",p->element.key);

    // left
    printPreOrder(p->pLeft);

    // Right
    printPreOrder(p->pRight);

} // end printInOrder
