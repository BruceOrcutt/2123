#include "BrowserList.h"


/******************** newBrowserList *************************************
BrowserList newBrowserList()
Purpose:
    Constructor for browser lists.
Parameters:
    None
Notes:
    Datastructure defined in BrowserList.h
Return value:
    Returns a new browser list. containing no nodes
**************************************************************************/
BrowserList newBrowserList()
{
    BrowserList pNew;   // the new Browser list we will return

    pNew = (BrowserList)malloc(sizeof(BrowserListImp));

    // Allocate the new list
    pNew->list = newDoublyLinkedList();

    // No nodes, so no current yet.
    pNew->pCurrentURL =  NULL;

    // return the list we just created
    return(pNew);

} // end newBrowserList

/******************** freeBrowserList *************************************
void freeBrowserList(BrowserList browserList)
Purpose:
    Destructor for browser lists.
Parameters:
    I/O browserList frees the passed in browser list.
Notes:
    Datastructure defined in BrowserList.h
Return value:
    Returns a new browser list. containing no nodes
**************************************************************************/
void freeBrowserList(BrowserList browserList)
{
    // clean up the DLL
    freeDoublyLinkedList(browserList->list);

    // Now clean up the browserList control node
    free(browserList);

    return;
} // end freeBrowserList


/******************** goToURL *********************************************
void goToURL(BrowserList browserList, Element element)
Purpose:
    Add a new URL to the list.
Parameters:
    I   element     URL to add to the list
    I/O browserList New URL added to the list.
Notes:
    Data structure defined in BrowserList.h
    Adds URL at current pointer's next pointer.
    If current's next points to values, list at that point is freed.
Return value:
    None.
**************************************************************************/
void goToURL(BrowserList browserList, Element element)
{
    NodeDL      *pNew;          // New node to add to the list
    BrowserList  pTempList;     // Used to clear out nodes

    // get a new node to add, stuffed with the URL
    //pNew = allocateNode(element);

    // See if current pointer has a next,
    if(browserList->pCurrentURL != NULL)
    {
        // if yes, create a new browser list, starting at current next
        // Then free it

        // first create a temp list
        pTempList = newBrowserList();

        // Set the head to be the next of current
        pTempList->list->pHead = browserList->pCurrentURL->pNext;

        // Set the foot to be the list's foot
        pTempList->list->pFoot = browserList->list->pFoot;

        // then free the new temp list to get rid of the next and all that follows
        freeBrowserList(pTempList);

        browserList->pCurrentURL->pNext = NULL;
        browserList->list->pFoot = browserList->pCurrentURL;
    } // end if current

    // Add new URL/node
    append(browserList->list,element);

    // Now move the current
    browserList->pCurrentURL = browserList->list->pFoot;

    return;
} // end goToURL

/******************** back ************************************************
back(BrowserList browserList)
Purpose:
    Go back to the webpage prior to the current webpage in the BrowserList.
Parameters:
    I/O browserList Updated to change the current pointer
Notes:
    The only thing that changes is the current pointer
Return value:
    TRUE:    If successful
    FALSE:   If not (e.g., if no such webpage exists).
**************************************************************************/
int back(BrowserList browserList)
{
    // Check to see if we have a previous item
    if(browserList->pCurrentURL->pPrev == NULL)
    {
        // no previous, so can't go back!
        return (FALSE);
    }

    // Otherwise, reset current pointer to the previous link
    browserList->pCurrentURL = browserList->pCurrentURL->pPrev;

    // We did it, so return true
    return (TRUE);
} // end back


/******************** forward *********************************************
forward(BrowserList browserList)
Purpose:
    Go forward to the webpage after the current webpage in the BrowserList.
Parameters:
    I/O browserList Updated to change the current pointer
Notes:
    The only thing that changes is the current pointer
Return value:
    TRUE:    If successful
    FALSE:   If not (e.g., if no such webpage exists).
**************************************************************************/
int forward(BrowserList browserList)
{
    // Check to see if we have a previous item
    if(browserList->pCurrentURL->pNext == NULL)
    {
        // no nexy, so can't go forward!
        return (FALSE);
    }

    // Otherwise, reset current pointer to the previous link
    browserList->pCurrentURL = browserList->pCurrentURL->pNext;

    // We did it, so return true
    return (TRUE);

} // end forward

/******************** printBrwowserList ***********************************
void printBrowserList(BrowserList browserList)
Purpose:
    Print the contents of the BrowserList, one webpage per line.
    Place *'s around the URL of the current webpage.
Parameters:
    I browserList The list of URLs to print
Notes:
    Starts at the head, goes until we hit the foot
Return value:
    None
**************************************************************************/
void printBrowserList(BrowserList browserList)
{
    char    cCur;   // Used to print, or not print, * for current item
    NodeDL *pTemp;  // Node pointer, to traverse the list to print

    // set the inital value as the head of the list
    pTemp = browserList->list->pHead;

    // Show the start of the list
    printf("----URL List-----\n");

    // go until we are out of values
    while(pTemp != NULL)
    {
        // check to see if we need the * around the current item
        if(pTemp == browserList->pCurrentURL)
        {
            cCur = '*';
        } else // otherwise, we'll print a space
        {
            cCur = ' ';
        }

        // Now print the current URL
        printf("%c%s%c\n",cCur,pTemp->element.szURL,cCur);

        // iterate to the next
        pTemp = pTemp->pNext;
    } // end while

    // Show the end of the list
    printf("----End List-----\n\n");

} // end printBrowserList

