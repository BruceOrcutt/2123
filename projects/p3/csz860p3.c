#include "BrowserList.h"


/**********************************************************************
csz860p3.c  p3  Bruce A. Orcutt

 Purpose:
    The program simulates a user going to various URLs in a browser

 Command Parameters:
    None. Program expects input via standard input.
Input:
    Input is via standard input.

    There are four categories of expected input:
        PRINT   Print the current list of URLS
        BACK    Set the current pointer back URL in the list
        FORWARD Set the current pointer forward one URL in the list
        URL     Any other input is considered to be a URL & added to the list

Results:
    When a PRINT command is issued, the URLS are printed, one by one.

    A "current" URL is kept track of.  This emulates what the browser user
    would currently see.

    When a PRINT is issued, the current URL is surrounded by the '*'
    character.

    If a BACK or a FORWARD is issued, when no such URLs exist in the list,
    the program will continue, but a warning message will appear.

Returns:
    List the return codes and meanings.

Notes:
    Description of any special information around usage.
**********************************************************************/
int main()
{
    char        szURL[MAX_URL_LENGTH];  // The URL to be read
    BrowserList browserList;            // The list of URLs we go to
    Element     element;                // A new element, a URL to add

    // Allocate the Browser List
    browserList = newBrowserList();

    // keep reading from standard input until we run out
    while(fgets(szURL,MAX_URL_LENGTH,stdin) != NULL)
    {
        // What we do depends on what we read

        // Chomp the \n, if it exists
        if(szURL[strlen(szURL)-1] == '\n')
            szURL[strlen(szURL)-1] = '\0';

        // Print URL list
        if(strcmp(szURL,"PRINT") == 0)
        {
            printBrowserList(browserList);
        }

        // Go back one
        else if(!strcmp(szURL,"BACK"))
        {
            if (!back(browserList))
                printf("WARNING: You cannot go back when you have no history\n\n");
        }

        // Go forward one
        else if(!strcmp(szURL,"FORWARD"))
        {
            if(!forward(browserList))
                printf("WARNING: You cannot go forward.  You have no URLs ahead of the current position.\n\n");
        }

        // URL
        else
        {
            strcpy(element.szURL,szURL);
            goToURL(browserList,element);
        }
    } // end reading loop

    // Clear out the URL list
    freeBrowserList(browserList);

    return 0;
}

