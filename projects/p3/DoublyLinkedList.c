#include "DoublyLinkedList.h"
// This file contains the impelementation of the doubly linked list


/******************** newDoublyLinkedList **********************************
 DoublyLinkedList newDoublyLinkedList()

Purpose:
    Constructor for a Doubly Linked List.  Returns a new Doubly Linked List,
    head and foot at NULL.
Parameters:
    None
Notes:
    Data structure is defined in DoublyLinkedList.h
Return value:
    A new DoublyLinkedList, with head and foot set to NULL.
**************************************************************************/
DoublyLinkedList newDoublyLinkedList()
{
    DoublyLinkedList pTemp; // the new linked list we will return

    // allocate the list
    pTemp = (DoublyLinkedList) malloc(sizeof(DoublyLinkedListImp));

    // set the pointers
    pTemp->pHead = NULL;
    pTemp->pFoot = NULL;

    // return the list header
    return (pTemp);
} // end newDoublyLinkedList

/******************** isEmpty **********************************
 int isEmpty()

Purpose:
    Detects an empty linked list.  If pHead is NULL, returns TRUE, otherwise FALSE.
Parameters:
    I list  The linked list to check
Notes:
    Data structure is defined in DoublyLinkedList.h

Return value:
    TRUE:   if the list exists, but has no nodes
    FALSE:  if the list exists, and has at least one node
    ERROR:  if the pointer to the list is NULL
**************************************************************************/
int isEmpty(DoublyLinkedList list)
{
    // if we get passed a list that hasn't even been through new list yet, error out
    if(list == NULL) return (ERROR);

    // if head is null, we have no nodes, so true
    if(list->pHead == NULL) return (TRUE);

    // if we get here, we have at least one node.
    return (FALSE);
} // end isEmpty

/******************** freeDoublyLinkedList **********************************
void freeDoublyLinkedList(DoublyLinkedList list)
Purpose:
    Destructor for an allocated doubly linked list
Parameters:
    I/O list    The list to be removed.  Will end with value of NULL
Notes:
    Data structure defined in DoublyLinkedList.h.
Return value:
    None
**************************************************************************/
void freeDoublyLinkedList(DoublyLinkedList list)
{
    NodeDL  *pCur,*pNext;  // for traversing the list

    // first check to see if we were passed NULL, if so, we're done.
    if(list == NULL) return;

    // is the list empty? then just free the control node
    if(isEmpty(list))
    {
        free(list);
        return;
    }

    // Otherwise we traverse the list, freeing as we go.
    for(pCur = list->pHead; pCur != NULL; pCur = pNext)
    {
        pNext = pCur->pNext;  // save the link to the next node
        free(pCur);
    }

    // now free the control node
    free(list);
} // end of freeDoublyLinkedList

/******************** allocateNode **********************************
NodeDL *allocateNode(Element value)
Purpose:
    Allocates one node, and sets it's value
Parameters:
    I value The value to set within the element
Notes:
    Data structure defined in DoublyLinkedList.h.
    Used by append()
Return value:
    Returns a pointer to the newly allocated node
**************************************************************************/
NodeDL *allocateNode(Element value)
{
    NodeDL *pNew;  // our new node

    // create the new node
    pNew = (NodeDL *) malloc(sizeof(NodeDL));

    // set the value
    pNew->element = value;

    // set the pointers
    pNew->pNext = NULL;
    pNew->pPrev = NULL;

    return(pNew);

} // end allocateNode

/******************** append **********************************
void append(DoublyLinkedList list, Element value)

Purpose:
    Adds a node to the end of the list
Parameters:
    I/O list    The linked list to add a node to
    I   value   The value to add to the new node
Notes:
    Data structure defined in DoublyLinkedList.h.
    Uses allocateNode
Return value:
    Returns a pointer to the newly allocated node
**************************************************************************/
void append(DoublyLinkedList list, Element value)
{

    NodeDL *cur;    // new node

    // allocate the new node
    cur = allocateNode(value);

    // do we have an existing list?
    if(!isEmpty(list))
    {
        // Now set the prev, the old foot. Next is fine at NULL
        cur->pPrev = list->pFoot;

        // add the node after the foot
        list->pFoot->pNext = cur;
        list->pFoot = cur;
    } else // otherwise, this is our first node!
    {
        list->pHead = cur;
        list->pFoot = cur;
    }
} // end append

